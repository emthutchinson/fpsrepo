﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public PlayerHealth playerHealth;
	public GameObject enemy;
    public int maxEnemy = 10;
    public float spawntime = 5f;

    float timer = 0f;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("Spawn", 0, spawntime);
	}

    private void Update()
    {
        timer += Time.deltaTime;
    }
	
	void Spawn () {
		if (playerHealth.currentHealth <= 0f || timer < spawntime ||  EnemyHealth.enemyCount >= maxEnemy) {
			return;
		}
        timer = 0f;
		Instantiate (enemy, transform.position, transform.rotation);
	}
}
