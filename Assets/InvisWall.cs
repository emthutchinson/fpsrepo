﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisWall : MonoBehaviour {
    public GameObject enemy;

	// Use this for initialization
	void Start () {
        Physics.IgnoreCollision(enemy.GetComponent<Collider>(), GetComponent<Collider>());
	}
}
