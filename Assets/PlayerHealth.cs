﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerHealth : MonoBehaviour
{
    public int StartingHealth = 100;
    public int currentHealth;
    public Text healthText;
    public AudioClip deathClip;
    public Image damageImage;
    public float flashSpeed = 5f;
    public Color flashColor = new Color(1f, 0f, 0f, 0.1f);
    public GameObject gameOverPanel;


    AudioSource playerAudio;
    FirstPersonController playerController;
    PlayerShooting playerShooting;
    bool isDead;
    bool damaged;

    void Awake()
    {
        playerAudio = GetComponent<AudioSource>();
        playerController = GetComponent<FirstPersonController>();
        playerShooting = GetComponentInChildren<PlayerShooting>();
        currentHealth = StartingHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (damaged && !isDead)
        {
            damageImage.color = flashColor;
        }
        if (!damaged && !isDead)
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
    }

    public void TakeDamage(int amount)
    {
        Debug.Log("Player taking damage");

        damaged = true;
        currentHealth -= amount;
        if (currentHealth >= 0)
        {
            healthText.text = "Health: " + currentHealth.ToString();
        }
        else
        {
            healthText.text = "Health: 0";
        }
        // playerAudio.Play();
        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }

    void Death()
    {
        isDead = true;



        playerController.enabled = false;
        playerShooting.enabled = false;

        damageImage.color = new Color(0f, 0f, 0f, 0f);
        StartCoroutine("Fade");

    }

    IEnumerator Fade()
    {
        for (float f = 0f; f <= 1; f += 0.05f)
        {
            Color c = new Color(0f, 0f, 0f, 0f);
            c.a = f;
            damageImage.color = c;
            Debug.Log(c.a);
            yield return null;
        }
        damageImage.color = new Color(0f, 0f, 0f, 1f);
        playerAudio.clip = deathClip;
        playerAudio.Play();
        gameOverPanel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

    }
}