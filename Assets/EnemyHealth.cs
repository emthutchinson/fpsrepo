﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {
    public static int enemyCount = 0;
    public static int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;


    AudioSource enemyAudio;
    MeshCollider meshCollider;
    float timer = 0f;
    int value;
    bool isDead;
    bool isSinking;
    
	void Awake () {
        enemyAudio = GetComponent<AudioSource>();
        meshCollider = GetComponent<MeshCollider>();
        enemyCount += 1;
        currentHealth = startingHealth;
        value = startingHealth;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
		if(isSinking)
        {
            transform.Translate(-Vector3.up * sinkSpeed * Time.deltaTime);
        }
        if (timer > 10f)
        {
            //timer = 0f;
            //enemyAudio.Play();
        }
	}

    public void TakeDamage (int amount)
    {
        if (isDead)
            return;

        // enemyAudio.Play();

        currentHealth -= amount;

        if (currentHealth <=0)
        {
            Death();
        }
    }

    void Death ()
    {
        startingHealth += 5;
        Debug.Log("Starting Health: " + startingHealth);
        isDead = true;
        meshCollider.isTrigger = true;
        enemyCount -= 1;
        Debug.Log("enemy count: " + enemyCount);
        StartSinking();
        // animator.SetTrigger("Dead");
    }

    public void StartSinking ()
    {
        ScoreKeeper.score += value;
        GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        isSinking = true;

        Destroy(gameObject, 3f);
    }
}
