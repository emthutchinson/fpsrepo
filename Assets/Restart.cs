﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour {


    public void RestartGame ()
    {
        Debug.Log("Restart started");
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
        EnemyHealth.enemyCount = 0;
        EnemyHealth.startingHealth = 100;
        
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
